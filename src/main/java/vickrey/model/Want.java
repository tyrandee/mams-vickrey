package vickrey.model;

public class Want {
    private String id;
	private String item;
	private int quantity;
	private int maxPrice;
	private String state;

    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(int maxPrice) {
		this.maxPrice = maxPrice;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String toString() {
    	return "Item: " + item;
	}
}
