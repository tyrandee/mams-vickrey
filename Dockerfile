FROM openjdk:latest
WORKDIR '/mams'
COPY ./target/mams-vickrey-1.0-SNAPSHOT-jar-with-dependencies.jar mams-vickrey-1.0-SNAPSHOT-jar-with-dependencies.jar
ENTRYPOINT ["java", "-jar", "mams-vickrey-1.0-SNAPSHOT-jar-with-dependencies.jar"]